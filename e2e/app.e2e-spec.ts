import { RssAppPage } from './app.po';

describe('rss-app App', () => {
  let page: RssAppPage;

  beforeEach(() => {
    page = new RssAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
