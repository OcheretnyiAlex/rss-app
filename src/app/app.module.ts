import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FeedService} from './services/feed.service';

import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {HeaderComponent} from './components/header/header.component';
import {ActiveMessageService} from './services/active-message.service';
import {ChannelsUrlService} from './services/channels-url.service';
import {ActiveChannelService} from './services/active-channel.service';
import {ChannelsService} from './services/channels.service';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    FeedService,
    ActiveMessageService,
    ChannelsUrlService,
    ActiveChannelService,
    ChannelsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
