import {Injectable} from '@angular/core';
import * as Parser from 'rss-parser';
import {environment} from '../../environments/environment';
import {from} from 'rxjs/observable/from';
import {Observable} from 'rxjs/Observable';
import {Channel} from '../models/Channel';

@Injectable()
export class FeedService {
  private parser: Parser;

  constructor() {
    this.parser = new Parser();
  }

  getContentFeed(url: string): Observable<Channel> {
    return from(this.parser.parseURL(environment.CORS_PROXY + url));
  }
}
