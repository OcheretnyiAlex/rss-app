import {Injectable} from '@angular/core';
import {Feed} from '../models/Feed';

@Injectable()
export class ActiveMessageService {
  private message;

  setMessage(message: Feed) {
    this.message = message;
  }

  getMessage() {
    return this.message;
  }
}
