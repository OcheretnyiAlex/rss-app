import { Injectable } from '@angular/core';
import {Channel} from '../models/Channel';;

@Injectable()
export class ChannelsService {

  private channels: Channel[];

  getChannels() {
    return this.channels;
  }

  setChannels(channels: Channel[]) {
    this.channels = channels;
  }

}
