import { Injectable } from '@angular/core';
import {DefaultChannels} from '../channels';

@Injectable()
export class ChannelsUrlService {

  private channels: string[] = DefaultChannels.channels;
  constructor() { }

  getChannels() {
    return this.channels;
  }
  addNewChannel(channel: string) {
    this.channels.push(channel);
  }
}
