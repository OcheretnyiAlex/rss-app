import {Injectable} from '@angular/core';
import {Channel} from '../models/Channel';

@Injectable()
export class ActiveChannelService {
  private activeChannel: Channel;

  getActiveChannel(): Channel {
    return this.activeChannel;
  }

  setActiveChannel(channel: Channel) {
    this.activeChannel = channel;
  }


}
