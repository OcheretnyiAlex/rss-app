import {SrcData} from './SrcData';
import {Feed} from './Feed';

export class Channel {
  image: SrcData;
  items: Feed[];
  title: string;
  link: string;
  pubDate: Date;
}
