import {Addition} from './Addition';

export class Feed {
  content: string;
  enclosure: Addition;
  pubDate: string;
  link: string;
  title: string;

}
