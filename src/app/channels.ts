export interface Channels {
  channels: string[];
}

export const DefaultChannels: Channels = {
  channels: [
    'https://www.liga.net/tech/technology/rss.xml',
    'https://www.liga.net/news/sport/rss.xml',
    'https://www.liga.net/tech/gadgets/rss.xml',
    'https://www.liga.net/news/politics/rss.xml',
  ]
};
