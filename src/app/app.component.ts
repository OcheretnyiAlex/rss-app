import {Component, OnInit} from '@angular/core';
import {FeedService} from './services/feed.service';
import {ActivatedRoute, Data, NavigationEnd, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {filter, map, mergeMap} from 'rxjs/operators';
import {Channel} from './models/Channel';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  channel: Channel;
  isPageFound: boolean;

  constructor(private feed: FeedService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private titleService: Title
  ) {

  }

  ngOnInit(): void {
    this.feed.getContentFeed('https://www.liga.net/tech/technology/rss.xml')
      .subscribe(result => {
        this.channel = result;
      });

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route: ActivatedRoute) => {
        while (route.firstChild) {
          route = route.firstChild
        }
        return route;
      }),
      filter((route: ActivatedRoute) => {
        return route.outlet === 'primary'
      }),
      mergeMap((route: Data) => route.data
      ))
      .subscribe((event) => {
        this.isPageFound = event['title'] !== 'PageNotFound';
        this.titleService.setTitle(event['title'])
      })
  }


}
