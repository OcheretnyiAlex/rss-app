import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'dashboard', data: {title: 'Home'}, children: [
      {
        path: 'channels',
        loadChildren: './components/channel-list/channel-list.module#ChannelListModule',
        data: {title: 'Channels'}
      },
      {
        path: 'message',
        loadChildren: './components/message/message.module#MessageModule',
        data: {title: 'Message'}
      },
      {
        path: 'statistics',
        loadChildren: './components/statistics/statistics.module#StatisticsModule',
        data: {title: 'Statistics'}
      }
    ]
  },
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent, data: {title: 'PageNotFound'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
