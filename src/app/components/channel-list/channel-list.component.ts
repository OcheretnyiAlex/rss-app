import {Component, OnInit} from '@angular/core';
import {FeedService} from '../../services/feed.service';
import {Channel} from '../../models/Channel';
import {ChannelsUrlService} from '../../services/channels-url.service';
import {ChannelsService} from '../../services/channels.service';

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.css']
})
export class ChannelListComponent implements OnInit {
  channels: Channel[] = [];
  newChannelUrl: string;
  channelAmount: number;
  isChannelStillLoading = true;
  private channelsUrls: string[];

  constructor(private feed: FeedService,
              private channel: ChannelsUrlService,
              private savedChannels: ChannelsService) {
  }

  ngOnInit() {
    if (this.savedChannels.getChannels()) {
      this.channels = this.savedChannels.getChannels();
      this.channelAmount = this.channels.length;
      this.isChannelStillLoading = false;
    } else {
      this.channelsUrls = this.channel.getChannels();
      this.channelAmount = this.channelsUrls.length;
      this.channelsUrls.forEach((url, index) => {
        this.feed.getContentFeed(url).subscribe(channel => {
          channel.link = url.slice(0, url.indexOf('/rss.xml'));
          this.channels.push(channel);
          if (this.channelAmount - 1 === index) {
            this.isChannelStillLoading = false;
            this.savedChannels.setChannels(this.channels);
          }
        });
      });
    }
  }

  addNewChannel(): void {
    console.log(this.newChannelUrl);
    if (this.newChannelUrl.indexOf('/rss.xml') === -1) {
      return
    }
    this.channel.addNewChannel(this.newChannelUrl);
    this.feed.getContentFeed(this.newChannelUrl).subscribe(
      channel => {
        channel.link = this.newChannelUrl.slice(0, this.newChannelUrl.indexOf('/rss.xml'));
        this.channels.unshift(channel);
        this.savedChannels.setChannels(this.channels);
      }
    )
  }

}
