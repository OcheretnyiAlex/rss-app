import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChannelListRoutingModule} from './channel-list-routing.module';
import { ChannelComponent } from './channel/channel.component';
import {ChannelListComponent} from './channel-list.component';
import {ActiveChannelService} from '../../services/active-channel.service';
import { ActiveChannelComponent } from './active-channel/active-channel.component';
import {FormsModule} from '@angular/forms';
import {ChannelsUrlService} from '../../services/channels-url.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChannelListRoutingModule,
  ],
  declarations: [
    ChannelComponent,
    ChannelListComponent,
    ActiveChannelComponent
  ]
})
export class ChannelListModule {
}
