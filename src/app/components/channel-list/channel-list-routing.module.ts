import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ChannelListComponent} from './channel-list.component';
import {ActiveChannelComponent} from './active-channel/active-channel.component';

const routes: Routes = [
  {
    path: 'all', component: ChannelListComponent
  },
  {
    path: 'active', component: ActiveChannelComponent, data: {title: 'Active channel'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChannelListRoutingModule {
}
