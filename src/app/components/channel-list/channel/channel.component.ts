import {Component, Input, OnInit} from '@angular/core';
import {ActiveChannelService} from '../../../services/active-channel.service';
import {Channel} from '../../../models/Channel';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {
  @Input() channel;
  @Input() title;
  @Input() link;
  @Input() channelImg;

  constructor(private activeChannel: ActiveChannelService) { }

  ngOnInit() {
  }

  setSelectedChannel(channel: Channel): void {
      this.activeChannel.setActiveChannel(channel);
  }

}
