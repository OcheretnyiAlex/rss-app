import {Component, OnInit} from '@angular/core';
import {ActiveChannelService} from '../../../services/active-channel.service';
import {Channel} from '../../../models/Channel';
import {Feed} from '../../../models/Feed';
import {ActiveMessageService} from '../../../services/active-message.service';

@Component({
  selector: 'app-active-channel',
  templateUrl: './active-channel.component.html',
  styleUrls: ['./active-channel.component.css']
})
export class ActiveChannelComponent implements OnInit {
  isChannelChosen = true;
  channel: Channel;
  messageAmount: number;

  constructor(private activeChannel: ActiveChannelService,
              private activeMessage: ActiveMessageService) {
  }

  ngOnInit() {
    this.channel = this.activeChannel.getActiveChannel();
    if (this.channel) {
      this.messageAmount = this.channel.items.length;
    } else {
      this.isChannelChosen = false;
    }
  }

  setActiveMessage(message: Feed) {
    this.activeMessage.setMessage(message);
  }

}
