import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChannelsService} from '../../services/channels.service';
import {ActiveChannelService} from '../../services/active-channel.service';
import {ActiveMessageService} from '../../services/active-message.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit, AfterViewInit {

  @ViewChild('messageCanvas') statistics: ElementRef;
  canvas: HTMLCanvasElement;
  channelsCount: number;
  messagesCount: number;
  messageContent: string;
  allMessageLetters: string;
  latinLetters: string;
  allLetterPercent: number;
  latinLettersPercent: number;
  isChannelsLoaded = true;
  isChannelChosen = true;
  isMessageChosen = true;

  constructor(private channels: ChannelsService,
              private messages: ActiveChannelService,
              private message: ActiveMessageService) {
  }

  ngOnInit() {
    this.channels.getChannels() ?
      this.channelsCount = this.channels.getChannels().length : this.isChannelsLoaded = false;
    this.messages.getActiveChannel() ?
      this.messagesCount = this.messages.getActiveChannel().items.length : this.isChannelChosen = false;
    if (this.message.getMessage()) {
      let regexp = new RegExp('[a-zа-яіїё]+', 'g');
      this.messageContent = this.message.getMessage().content.toLowerCase();
      this.allMessageLetters = this.messageContent
        .match(regexp)
        .reduce((prev, curr) => {
          return prev + curr
        });
      regexp = new RegExp('[a-z]+', 'g');
      if (this.messageContent.match(regexp)) {
        this.latinLetters = this.messageContent
          .match(regexp)
          .reduce((prev, curr) => {
            return prev + curr
          });
      }
      this.allLetterPercent = this.getAllLettersPercent();
      this.latinLettersPercent = this.getLatinLettersPercent();
    } else {
      this.isMessageChosen = false;
    }
  }

  ngAfterViewInit(): void {
    if (this.isMessageChosen) {
      this.canvas = (<HTMLCanvasElement>this.statistics.nativeElement);
      this.drawChart({
        canvas: this.canvas,
        data: [
          this.allMessageLetters,
          this.latinLetters ? this.latinLetters : 0
        ],
        colors: ['#f16e23', '#fde23e']
      })();
    }
  }

  private getAllLettersPercent() {
    return (this.allMessageLetters.length - (this.latinLetters ? this.latinLetters.length : 0)) *
      100 / this.allMessageLetters.length
  }

  private getLatinLettersPercent() {
    return this.latinLetters ? this.latinLetters.length : 0;
  }

  private drawChartPiece(ctx, centerX, centerY, radius, startAngle, endAngle, color) {
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.closePath();
    ctx.fill();
  }

  private drawChart(options) {
    const canvas = options.canvas;
    const ctx = canvas.getContext('2d');
    const data = options.data;
    const colors = options.colors;
    return () => {
      let total_value = 0;
      let color_index = 0;
      for (const letters of data) {
        if (letters) {
          const value = letters.length;
          total_value += value;
        }
      }
      let start_angle = 0;
      for (const letters of data) {
        if (letters) {
          const value = letters.length;
          const slice_angle = 2 * Math.PI * value / total_value;
          this.drawChartPiece(ctx,
            this.canvas.width / 2,
            this.canvas.height / 2,
            Math.min(this.canvas.width / 2, this.canvas.height / 2),
            start_angle,
            start_angle + slice_angle,
            colors[color_index % colors.length]);
          start_angle += slice_angle;
          color_index++;
        }
      }
    }
  }


}
