import {Component, OnInit} from '@angular/core';
import {ActiveMessageService} from '../../services/active-message.service';
import {Feed} from '../../models/Feed';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  currentMessage: Feed;
  publicationDate: Date;
  isMessageChosen = true;

  constructor(private message: ActiveMessageService) {
  }

  ngOnInit() {
    this.currentMessage = this.message.getMessage();
    if (this.currentMessage) {
      this.publicationDate = new Date(this.currentMessage.pubDate);
    } else {
      this.isMessageChosen = false;
    }
  }


}
